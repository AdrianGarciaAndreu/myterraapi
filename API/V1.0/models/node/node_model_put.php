<?php

/**
 * Puts a new node on the BD
 * @author Adrián García
 */


// Parametros recibidos
$guid = $body_params["guid"];
$hr = $body_params["hr"];
//$hwMomment = $body_params["hwMomment"];
$id = $body_params["id"];
$ledColorR = $body_params["ledColorR"];
$ledColorG = $body_params["ledColorG"];
$ledColorB = $body_params["ledColorB"];
$ledState = $body_params["ledState"];
$lightInt = $body_params["lightInt"];
$moist = $body_params["moist"];
//$momment = $body_params["momment"];
$name = $body_params["name"];
$nodeIsConnected = $body_params["nodeIsConnected"];
$statusRequest = $body_params["statusRequest"];
$temp = $body_params["temp"];
$uvState = $body_params["uvState"];
$wInt = $body_params["wInt"];


$sql = "INSERT INTO nodes (guid, hr, hwMomment, macAddress, ledColorR, ledColorG, ledColorB, ";
$sql .= "ledState, lightInt, moist, momment, name, nodeIsConnected, statusRequest, temp, ";
$sql .= "uvState, wInt)  VALUES('".$guid."', ".$hr.", CURRENT_TIMESTAMP, '".$id."', ".$ledColorR.", ";
$sql .= "".$ledColorG.", ".$ledColorB." , ".$ledState.", ".$lightInt.", ".$moist.", ";
$sql .= " CURRENT_TIMESTAMP, '".$name."', ".$nodeIsConnected.", ".$statusRequest.", ".$temp.", ";
$sql .= "".$uvState.", ".$wInt.")";

//echo $sql;

$resultado = mysqli_query($conexion, $sql);

// Almacena la respuesta en un array asociativo
$respuesta = $resultado;


?>